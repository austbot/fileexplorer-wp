module.exports = function(grunt) {
  var pjson = require('./package.json');

  grunt.initConfig({
    browserify: {
      build: {
        files: {
          'build/admin.js': ['coffee/admin.coffee'],
          'build/client.js': ['coffee/client.coffee']
        },
        options: {
          transform: ['coffeeify', 'hbsfy']
        }
      }
    },
    sass: {
      dist: {
        files: {
          'build/admin.css': ['scss/admin.scss'],
          'build/client.css': ['scss/client.scss']
        }
      }
    },
    watch: {
      files: ["scss/**/*", "coffee/**/*"],
      tasks: ['sass', 'browserify:build']
    },
    bump: {
      options: {
        files: ['package.json', 'composer.json'],
        commit: false,
        createTag: false,
        push: false,
      }
    },
    exec: {
      package: {
        cmd: "tar --exclude-from=.package-ignore -pczf release-" + pjson.version + ".tgz * .??*"
      }
    }
  });
  
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-exec');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-bump');
  grunt.registerTask('build', ['sass', 'browserify:build']);
  grunt.registerTask('package', ['build', 'exec:package']);
};
