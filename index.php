<?php
defined('ABSPATH') or die("No script kiddies please!");
/*
Plugin Name: File Explorer
*/

define('FE_ROOT_PATH', __FILE__);
define('FE_ROOT_DIR', __DIR__);
define('FE_AJAXURL', admin_url( 'admin-ajax.php' ));
define('FE_UPLOAD_PATH', wp_upload_dir()['basedir']."/file-explorer");
define('FE_UPLOADS_URL', wp_upload_dir()['baseurl']."/file-explorer");
//Composer Deps.
if(!defined('COMPOSER_ROOT'))require_once 'vendor/autoload.php';
//FE
require_once __DIR__."/plugin/Fe.php";
//Custom Helpers.
require __DIR__ . '/plugin/helpers.php';
//Short Codes
require_once __DIR__."/plugin/shortcodes.php";

FileExplorer::init(FE_AJAXURL, FE_UPLOAD_PATH);

//Only Execute in the Admin interface.
if(is_admin()){
  helpers::addStyle('FEWP-admin-style', plugins_url( 'build/admin.css', FE_ROOT_PATH ), true);
  helpers::addScript('FEWP-admin-script', plugins_url( 'build/admin.js', FE_ROOT_PATH ), true);
  add_action('admin_menu', function(){
    $page = add_menu_page( 'File Explorer', 'File Explorer', 'manage_options', 'file-explorer' , array('FileExplorer', 'admin'));
  });
  add_action('media_buttons_context',  function(){
    FileExplorer::admin();
    echo '<button type="button" id="insert-documents" class="button insert-documents" data-editor="content" title="Add Documents">Add Documents</button>';
  });
  //Register Ajax Functions
  helpers::addAjaxAction("fe_admin_upload", array('FileExplorer', 'upload'));
  helpers::addAjaxAction("fe_admin_delete", array('FileExplorer', 'delete'));
  helpers::addAjaxAction("fe_admin_create_dir", array('FileExplorer', 'create_directory'));
  helpers::addAjaxAction("fe_admin_listing", array('FileExplorer', 'admin_listing'));
  helpers::addAjaxAction("fe_client_listing", array('FileExplorer', 'client_listing'));
  helpers::addAjaxAction("fe_client_listing", array('FileExplorer', 'client_listing'), false);
}
else{
  helpers::addScript('FEWP-client-script', plugins_url( 'build/client.js', FE_ROOT_PATH ), false, false);
  helpers::addStyle('FEWP-client-style', plugins_url( 'build/client.css', FE_ROOT_PATH ));
}
