AjaxPromise = require 'ajax-promise'

module.exports = {
  listing: (path, callback) ->
    url = window.url_root+"?action=fe_"+window.env+"_listing"
    url += "&path="+path if path != "/"
    AjaxPromise
      .get url
      .then (response) ->
        callback response if callback
      .catch (err) ->
        console.log 'Listing: errors in response', err
  delete: (path, type, callback) ->
    url = window.url_root+"?action=fe_admin_delete"
    AjaxPromise
      .post url, {path: path, type: type}
      .then (response) ->
        callback response if callback
      .catch (err) ->
        console.log 'Delete: errors in request', err
  createDir: (path, name, callback) ->
    url = window.url_root+"?action=fe_admin_create_dir"
    AjaxPromise
      .post url, {path: path, name: name}
      .then (response) ->
        callback response if callback
      .catch (err) ->
        console.log 'Create Dir: errors in request', err
}
