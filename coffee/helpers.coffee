Handlebars = require 'hbsfy/runtime'

Handlebars.registerHelper 'truncate', (str, num) ->
  return if !str
  if str.length > num
    new_str = str.substr(0, num-3) + '...'
    return new_str
  else return str

Handlebars.registerHelper 'isEqual', (options) ->
  if options.hash.test == options.hash.control
    options.fn(this)
  else
    options.inverse(this)

Handlebars.registerHelper 'formatMoment', (date, format) ->
  date.format(format)
