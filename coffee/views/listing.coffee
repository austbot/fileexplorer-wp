View = require 'ampersand-view'
listingService = require '../listingService.coffee'
fileTemplate = require "../templates/file.hbs"
dirTemplate = require "../templates/dir.hbs"

templates = {
  'dir': dirTemplate,
  'file': fileTemplate
}

Listing = View.extend {
  render: (opts) ->
    this.renderWithTemplate this, templates[@model.type]
    return this
  events: {
    'mouseenter': 'showActions',
    'mouseleave': 'showActions',
    'click [data-hook=delete]': 'delete',
    'click [data-hook=fileName]': 'editorEmbed'
  },
  editorEmbed: ->
    editor = window.tinyMCE
    if editor && editor.activeEditor
      editor.activeEditor.selection.setContent "[fileLink path=\"#{encodeURI @model.path}\"]#{@model.basename}[/fileLink]"
      @el.classList.add 'checked'
  delete: ->
     if confirm "Are you sure you want to delete #{@model.basename} ?"
       @model.collection.remove @model
       listingService.delete @model.path , @model.type
  showActions: ->
    actions = this.queryByHook 'actions'
    actions.classList.toggle 'shown'

}
module.exports = Listing
