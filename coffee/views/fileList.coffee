View = require 'ampersand-view'
listingService = require '../listingService.coffee'
fileListTemplate = require '../templates/fileList.hbs'

FileSystem = View.extend {
  template: fileListTemplate
  events: {
    'keyup [data-hook=filter]': (e) ->
      this.filter() if e.keyCode is 13
    'click [data-hook=filterButton]': 'filter',
    'click [data-hook=clearFilter]': (e) ->
      @model.filterStr = ''
      this.render()
  },
  filter: ->
    input = this.queryByHook 'filter'
    @model.filterStr = input.value
    this.render()
  getListings: ->
    cb = (response) =>
      @model.listings.reset response.listing
      @model.listings.sort()
      this.render()
    listingService.listing this.model.currentPath, cb
  initialize: ->
    this.getListings()
    return this
}
module.exports = FileSystem
