View = require 'ampersand-view'
ListingView = require './listing.coffee'
UploadView = require './upload.coffee'
listingService = require '../listingService.coffee'
ddZone = require 'drag-and-drop-files'
Upload = require 'component-upload'
fsTemplate = require '../templates/fileSystem.hbs'

FileSystem = View.extend {
  template: fsTemplate
  render: (opts) ->
    this.renderWithTemplate this
    listingCollectionEl = this.queryByHook 'listingCollection'
    uploadCollectionEl = this.queryByHook 'uploads'
    this.renderCollection @model.listings, ListingView, listingCollectionEl, opts if @model.listings
    this.renderCollection @model.uploads, UploadView, uploadCollectionEl, opts
    this.setupDropZone()
    return this
  events: {
    'click [data-href]': 'setPath',
    'click [data-hook=createDir]': 'createDir'
  },
  setPath: (e) ->
    if e
      this.model.currentPath = e.delegateTarget.getAttribute 'data-href'
    else
      this.model.currentPath = '/'
    this.getListings()
  setupDropZone: ->
    ddZone this.el, (files) =>
      @model.uploads.reset()
      numFiles = files.length > 1
      ended = 0
      files.map (file, index) =>
        fileKey = encodeURI(file.name)
        uploadModel = @model.uploads.add {
          id: fileKey,
          name: file.name,
          size: file.size,
          type: file.type,
          progress: 0,
          error: false,
          complete: false
        };
        upload = new Upload file
        upload.to {
          path: window.url_root+"?action=fe_admin_upload",
          data: {
            path: this.model.currentPath
          },
          headers: {
            'X-REQUESTED-WITH': 'XMLHttpRequest'
          }
        }
        upload.on 'progress', (e) =>
          uploadModel.progress = e.percent
        upload.on 'end', (req) =>
          result = JSON.parse req.response
          if result.success
            uploadModel.completed = true
          else
            uploadModel.error = true
            uploadModel.errorMessage = result.errors
          ended += 1
          this.getListings() if ended == files.length
  getListings: ->
    cb = (response) =>
      @model.listings.reset response.listing
      this.render()
    listingService.listing this.model.currentPath, cb
  createDir: ->
    dirInput = this.queryByHook 'dirName'
    if dirInput.value.length > 0
      cb = =>
        dirInput.value = ''
        this.getListings()
      listingService.createDir this.model.currentPath, dirInput.value, cb
  initialize: ->
    this.getListings()
    return this
}
module.exports = FileSystem
