View = require 'ampersand-view'
uploadTemplate = require "../templates/upload.hbs"

Uploads = View.extend {
  template: uploadTemplate,
  bindings: {
    'model.progress': {
      type: (el, value, previousValue) ->
        el.textContent = value+"%"
      hook: 'message'
    },
    'model.error': {
      type: (el, value, previousValue) ->
        if value
          el.parentNode.classList.toggle 'upload-error'
          el.textContent = @model.errorMessage
      hook: 'message'
    },
    'model.completed': {
      type: 'booleanClass',
      hook: 'upload',
      name: 'upload-complete'
    }
  }
}
module.exports = Uploads
