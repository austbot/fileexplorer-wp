State = require 'ampersand-state'
filesize = require 'filesize'
moment = require 'moment'
chrono = require 'chrono-node'
Listing = State.extend {
  props: {
    basename: 'string',
    dirname: 'string',
    filename: 'string',
    path: 'string',
    extension: 'string',
    timestamp: 'number',
    type: 'string',
    size: 'number',
    showActions: {type: 'boolean', default: false}
  },
  derived: {
    fileSize: {
      deps: ['size'],
      fn: ->
        filesize @size, {round: 0}
    },
    date: {
      deps: ['filename', 'timestamp'],
      fn: ->
        date = chrono.parseDate @filename
        returnDate = date
        if !date
          @dateFound = false
          returnDate = @timestamp * 1000
        else
          @dateFound = true
        moment(returnDate)
    },
    order: {
      deps: ['filename'],
      fn: ->
        matches = /#(\d+)/.exec(@filename)
        if matches && matches.length > 1
          res = parseInt(matches[1])
          return res if !isNaN res
        0
    }
  }
}

module.exports = Listing;
