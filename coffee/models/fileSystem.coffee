State = require 'ampersand-state'
Collection = require 'ampersand-collection'
ListingCollection = require './listingCollection.coffee'

FileSystem = State.extend {
  props: {
    currentPath: { type: 'string', default: '/' },
  },
  derived: {
    paths: {
      deps: ['currentPath'],
      fn: ->
        folders = if @currentPath == '/' then "" else @currentPath.split '/'
        paths = []
        paths.push {name:'Root', link:'/'}
        if folders instanceof Array
          folders.map (folder, index) =>
            linkSplit = folders.slice 0, index+1
            link = if linkSplit == '' then '/' else linkSplit
            paths.push {name: folder, link: linkSplit.join '/'}
        return paths
    }
  },
  collections: {
    listings: ListingCollection,
    uploads: Collection.extend {
      model: State.extend {
        props: {
          name: 'string',
          size: 'number',
          progress: 'number',
          completed: 'boolean',
          error: 'boolean',
          errors: 'string'
        }
      }
    }
  }
}
module.exports = FileSystem
