State = require 'ampersand-state'
ListingCollection = require './listingCollection.coffee'

FileList = State.extend {
  props: {
    currentPath: { type: 'string', setOnce: true, default: window.listingPath },
    filterStr: {type: 'string', default: ''}
  },
  derived: {
    filteredList: {
      deps: ['listings', 'filterStr'],
      fn: ->
        return @listings.filter (listing) =>
          listing.filename.indexOf(@filterStr) isnt -1
    }
  },
  collections: {
    listings: ListingCollection,
  }
}
module.exports = FileList
