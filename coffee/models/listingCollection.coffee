Collection = require 'ampersand-collection'
Listing = require './listing.coffee'

ListingCollection = Collection.extend {
  model: Listing,
  comparator: (a, b) ->
    ad = a.date
    bd = b.date
    if a.dateFound && b.dateFound
      return 0 if ad.isSame bd
      if ad.isAfter bd
        return -1
      else
        return 1
    return -1 if a.dateFound && !b.dateFound;
    return 1 if !a.dateFound && b.dateFound;
    return -1 if a.order > b.order
    return 1 if b.order > a.order
    return 0 if b.order == a.order
}
module.exports = ListingCollection;
