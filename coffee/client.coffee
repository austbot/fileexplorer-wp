require './helpers.coffee'
FileListView = require './views/fileList.coffee'
FileListModel = require './models/fileList.coffee'

document.addEventListener "DOMContentLoaded", () ->
  fileSystemEl = document.getElementById "file-explorer"
  flModel = new FileListModel()
  flView = new FileListView {model: flModel, el: fileSystemEl}
