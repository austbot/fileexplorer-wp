require './helpers.coffee'
FileSystemView = require './views/fileSystem.coffee'
FileSystemModel = require './models/fileSystem.coffee'
dialog = require 'dialog-component'

document.addEventListener "DOMContentLoaded", () ->
  #If we are on the editor screen.
  fileSystemEl = document.getElementById "file-explorer"
  fs = new FileSystemModel()
  fsView = new FileSystemView {model: fs, el: fileSystemEl}

  editor = window.tinyMCE
  editorDocButton = document.getElementById 'insert-documents'
  if editor and editorDocButton
    modal = dialog "Add Documents", fileSystemEl
    modal
      .on 'hide', ->
        document.body.classList.remove 'no-scroll'
      .addClass 'explorer-dialog'
      .closable()
      .overlay()
    editorDocButton.addEventListener "click", (e)->
      document.body.classList.add 'no-scroll'
      modal
      .overlay()
      .show()
