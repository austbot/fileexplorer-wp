<?php

add_shortcode( 'fileList', function($atts){
  wp_enqueue_script('FEWP-client-script');
  return FileExplorer::embed($atts['path']);
});

add_shortcode( 'fileLink', function($atts, $content=null){
  $base = FE_UPLOADS_URL."/".urldecode($atts['path']);
  $safe_content = strip_tags($content);
  return "<a href='$base' title='$safe_content' target='_blank'>$safe_content</a>";
});
