<?php
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local as Adapter;
use Philo\Blade\Blade;

class FileExplorer
{
  private static $cache;
  private static $url_root;
  private static $views;
  private static $filesystem;

  public static function init($url_root, $file_base)
  {
    self::$url_root = $url_root;
    self::$filesystem = new Filesystem(new Adapter($file_base));
  }

  public static function admin()
  {
    self::check_user();
    $template = self::view('layout');
    $template->with('url_root', self::$url_root);
    echo $template->render();
  }

  public static function embed($path)
  {
    $template = self::view('embed');
    $template->with('listingPath', $path);
    $template->with('url_root', self::$url_root);
    return $template->render();
  }

  public static function admin_listing()
  {
    self::check_user();
    $path = self::input('get', 'path', null);
    $top_list = self::listContents($path);
    usort($top_list, function ($a, $b)
    {
      $at = $a['type'];
      $bt = $b['type'];
      if ($at == $bt)
        return 0;
      if ($at == 'dir' && $bt != 'dir')
        return -1;
      if ($at != 'dir' && $bt == 'dir')
        return 1;
    });
    return json_encode(Array('listing' => $top_list));
  }

  public static function client_listing()
  {
    $path = $_GET['path'];

    $top_list = self::listContents($path);

    $item_list = array_filter($top_list, function ($item)
    {
      if ($item['type'] === 'dir')
        return false;
      else return true;
    });

    array_walk($item_list, function (&$item)
    {
      $item['path'] = FE_UPLOADS_URL . '/' . str_replace('#', '%23' ,$item['path']);
      return $item;
    });
    return json_encode(Array('listing' => array_values($item_list)));
  }

  public static function upload()
  {
    self::check_user();
    $path = self::input('post', 'path', null);
    $file = $_FILES['file'];
    //Upload
    $save_path = ($path ? $path . "/" : '') . $file['name'];
    $response = Array();
    $response['url'] = $save_path;
    try
    {
      $stream = fopen($file['tmp_name'], 'r+');
      self::$filesystem->writeStream($save_path, $stream);
      fclose($stream);
    } catch (Exception $e)
    {
      $response['errors'] = $e->getMessage();
    }
    if (isset($response['errors']))
      $response['success'] = false;
    else $response['success'] = true;
    return json_encode($response);
  }

  public static function delete()
  {
    self::check_user();
    $path = self::input('post', 'path', null);
    $type = self::input('post', 'type', 'file');

    $response = Array();
    //Delete
    try
    {
      if ($type == 'file')
        self::$filesystem->delete($path);
      else
        self::$filesystem->deleteDir($path);
      $response['success'] = true;
    } catch (Exception $e)
    {
      $response['errors'] = $e->getMessage();
      $response['success'] = false;
    }
    return json_encode($response);
  }

  public static function create_directory()
  {
    self::check_user();
    //Grab post data
    $path = self::input('post', 'path', '');
    $name = self::input('post', 'name', null);
    $dir = (strlen($path) > 0 && $path != '/') ? $path . "/" . $name : $name;

    $response = Array();
    try
    {
      self::$filesystem->createDir($dir);
      $response['success'] = true;
    } catch (Exception $e)
    {
      $response['errors'] = $e->getMessage();
      $response['success'] = false;
    }
    echo json_encode($response);
  }

  //Private

  private static function view($name)
  {
    self::$cache = FE_ROOT_DIR . '/views/viewcache';
    self::$views = FE_ROOT_DIR . '/views';
    $blade = new Blade(self::$views, self::$cache);
    $View = $blade->view();
    return $View->make($name);
  }

  private static function check_user()
  {
    if (!current_user_can('upload_files'))
    {
      header($_SERVER['SERVER_PROTOCOL'] . ' 403 Not Authorized', true, 403);
      die();
    }
  }

  private static function error_and_die($error, $code)
  {
    header($_SERVER['SERVER_PROTOCOL'] . $error, true, 500);
    die($error);
  }

  private static function listContents($path)
  {
    if ($path)
      return self::$filesystem->listContents($path);
    else return self::$filesystem->listContents();
  }

  private static function input($type, $key, $noval)
  {
    if ($type == 'post')
    {
      if (isset($_POST[$key]))
        return $_POST[$key];
      else return $noval;
    }
    else
    {
      if (isset($_GET[$key]))
        return $_GET[$key];
      else return $noval;
    }
  }
}
